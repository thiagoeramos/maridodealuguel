CREATE TABLE `log_mailSend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(50) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `emails` longtext NOT NULL,
  `created_in` datetime NOT NULL,
  `updated_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_in` datetime NOT NULL,
  `updated_in` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `gravity` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `created_in` datetime NOT NULL,
  `updated_in` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*
-- Query: SELECT * FROM maridoaluguel.user
LIMIT 0, 1000

-- Date: 2016-02-20 17:13
*/
INSERT INTO `user` (`id`,`name`,`email`,`password`,`created_in`,`updated_in`,`status_id`) VALUES (1,'Root','celsodesouza14@ig.com.br','46f94c8de14fb36680850768ff1b7f2a','2014-04-26 00:00:00','2014-05-10 23:07:14',1);


/*
-- Query: SELECT * FROM maridoaluguel.work
LIMIT 0, 1000

-- Date: 2016-02-20 17:14
*/
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (1,'PEDREIRO E CONSTRUÇÃO',1,'<p>Atua em todas as etapas da sua constru&ccedil;&atilde;o, indo desde a funda&ccedil;&atilde;o ao acabamento.</p>\n<p>Realizamos a prepara&ccedil;&atilde;o do terreno para receber o alicerce, constru&ccedil;&atilde;o de colunas e radie, constru&ccedil;&atilde;o por meio de blocos estruturais com amarra&ccedil;&atilde;o ou blocos comuns com colunas, coloca&ccedil;&atilde;o de portas e janelas, lajes, telhado, aplica&ccedil;&atilde;o de pisos, azulejos, revestimentos e porcelanato.</p>\n<p>Atuamos tamb&eacute;m com a constru&ccedil;&atilde;o de Muros, casas, reformas de Banheiro, manuten&ccedil;&atilde;o de pisos, rebocos, etc</p>\n<p>Conte com nossos profissionais da hora de construir e realizar o seu sonho!</p>','alvenaria-brick-delivery-1259650206_80(1).jpg','reformas_construction-of-wood-1387453987_621.jpg','2015-03-26 11:42:42','2016-02-20 14:08:10',0);
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (2,'ACABAMENTO E REFORMAS',1,'<p>Deixe sua casa, apartamento ou escrit&oacute;rio mais bonito e cheia de estilo.<br /><br />Na hora de fazer alguma amplia&ccedil;&atilde;o, inova&ccedil;&atilde;o, ou restaura&ccedil;&atilde;o, ou se voc&ecirc; simplesmente quer uma nova pintura, coloca&ccedil;&atilde;o de gesso acartonado, grafiato, massa corrida, ou trocar seu piso cer&acirc;mico, tanto de im&oacute;veis residenciais, quanto comerciais, conte com a MS REFORMAS.<br /><br />Tamb&eacute;m fazemos constru&ccedil;&atilde;o, adapta&ccedil;&atilde;o e manuten&ccedil;&atilde;o em churrasqueiras de alvenaria de qualquer tipo, reformas prediais e manuten&ccedil;&otilde;es preventivas do seu im&oacute;vel.</p>','quality-prestacao-de-servicos-pinturas-texturas-reformas-455476.jpg','reformas_work-on-the-construction-site-1387453964_83_banner1.jpg','2015-03-26 11:58:18','2016-02-20 13:28:00',1);
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (3,'ELÉTRICA E HIDRÁULICA',3,'<p>Conte conosco tamb&eacute;m na hora de realizar a sua instala&ccedil;&atilde;o el&eacute;trica e hidr&aacute;ulica. Temos servi&ccedil;os de encanador, eletricista, instala&ccedil;&atilde;o de ventilador de teto, lou&ccedil;as e metais sanit&aacute;rios, torneiras, duchas e chuveiros, prateleiras, pintura decorativa. Projetos el&eacute;tricos, projetos de ilumina&ccedil;&atilde;o e muito mais.<br /><br />Executamos todo o projeto para im&oacute;veis novos desde mudan&ccedil;as de pontos el&eacute;tricos, hidr&aacute;ulicos e tv, at&eacute; pintura, coloca&ccedil;&atilde;o de portas, pedras, cer&acirc;micas, pisos, porcelanato at&eacute; modifica&ccedil;&otilde;es el&eacute;tricas, de tens&atilde;o, projetos em gesso, sancas e ilumina&ccedil;&atilde;o decorativa.</p>','i327139.jpg','i3271391.jpg','2015-03-26 11:58:56','2016-02-20 13:24:56',1);
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (4,'INSTALAÇÃO DE AR CONDICIONADO SPLIT',0,'<p>&nbsp;. Instala&ccedil;&otilde;es Residenciais<br />Trazer grande praticidade para nossos clientes e parceiros esse &eacute; nosso objetivo.</p>\n<p>&nbsp;. Instalar Ar Condicionado</p>\n<p>Realizamos a instala&ccedil;&atilde;o com garantia e seguran&ccedil;a de seu aparelho de ar condicionado.</p>\n<p>&nbsp;. Manuten&ccedil;&atilde;o de Ar Condicionado</p>\n<p>Profissionais treinados e qualificados, para garantir a seguran&ccedil;a dos clientes.</p>\n<p>&nbsp;. Limpeza de Ar Condicionado</p>\n<p>Limpeza, higieniza&ccedil;&atilde;o e simpatia s&atilde;o pe&ccedil;as fundamentais de nossas franquias.</p>','qual-a-parte-negativa-desta-engenhoca-2.jpg','bbc7aa1c3f7e695026a916b81d76e4fa(1).jpg','2015-03-26 12:03:40','2016-02-20 14:07:33',1);
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (5,'PINTURA',4,'<h2>Pintura Residencial</h2>\n<p>Quer pintar seu im&oacute;vel com profissionalismo e praticidade? Fale ent&atilde;o com nossa equipe.</p>\n<h2>Pintura de Apartamentos</h2>\n<p>Realizar os servi&ccedil;os solicitados no menor prazo e com melhor qualidade.</p>\n<h2>Textura</h2>\n<p>Profissionais treinados e qualificados, para garantir a seguran&ccedil;a dos clientes.</p>\n<h2>Muros e Placas</h2>\n<p>Limpeza, higieniza&ccedil;&atilde;o e simpatia s&atilde;o pe&ccedil;as fundamentais de nossas franquias.</p>\n<h2>Pintura de Port&otilde;es</h2>\n<p>Pintura de port&otilde;es resid&ecirc;ncias, janelas e esquadrias com limpeza e qualidade.</p>\n<h2>Pintura de Fachadas</h2>\n<p>Tenha uma nova fachada para seu im&oacute;vel, qualidade no servi&ccedil;o e limpeza &eacute; aqui.</p>','como-iniciar-um-servico-de-pintura-residencial-300x229.jpg','como-iniciar-um-servico-de-pintura-residencial-300x2291.jpg','2015-03-26 12:07:06','2016-02-20 13:27:30',1);
INSERT INTO `work` (`id`,`name`,`gravity`,`description`,`image`,`thumbnail`,`created_in`,`updated_in`,`status_id`) VALUES (6,'HIDRÁULICA',5,'<p>Instala&ccedil;&otilde;es Hidr&aacute;ulicas</p>\n<p>Instalar V&aacute;lvulas Hydra</p>\n<p>Instala&ccedil;&atilde;o de Registros</p>\n<p>Manuten&ccedil;&atilde;o em Registros; B&oacute;ias</p>\n<p>Localiza&ccedil;&atilde;o de Vazamentos<br />Limpeza de Caixa d\'&Aacute;gua</p>','hidraulica_plumber-1_banner.jpg','hidraulica_plumber_thumb.jpg','2015-03-26 12:11:21','2016-02-20 14:10:07',1);
