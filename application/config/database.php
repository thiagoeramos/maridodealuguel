<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group   = ENVIRONMENT;
$active_record  = TRUE;

$db['development']['hostname']  = 'localhost';
$db['development']['username']  = 'root';
$db['development']['password']  = '123qwe';
$db['development']['database']  = 'maridoaluguel';
$db['development']['dbdriver']  = 'mysql';
$db['development']['dbprefix']  = '';
$db['development']['pconnect']  = TRUE;
$db['development']['db_debug']  = TRUE;
$db['development']['cache_on']  = FALSE;
$db['development']['cachedir']  = '';
$db['development']['char_set']  = 'utf8';
$db['development']['dbcollat']  = 'utf8_general_ci';
$db['development']['swap_pre']  = '';
$db['development']['autoinit']  = TRUE;
$db['development']['stricton']  = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */