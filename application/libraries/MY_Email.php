<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email
{
 	public function __construct()
    {
        parent::__construct();
    }

	public function envia($email, $assunto, $mensagem, $bcc = false,$email_contact)
	{
		$CI =& get_instance();
		
		$CI->load->model('log_model');
		
		$config['charset']      = "utf-8";
		$config['mailtype']     = "html";
		$config['newline']      = "\r\n";
		
		$this->initialize($config);
		
		$this->clear();
			
		$config['wordwrap']     = true;
		
		$this->to(trim(EMAIL_CONTATO));
		
		if($bcc){
			$this->bcc(array('thiagoevangelista.contato@live.com', $bcc));
		}
		
		$this->set_mailtype('html');
		$this->from('noreply@cersomaridodealuguel.com.br');
		$this->subject($assunto);
		$this->message($mensagem);
		
		$CI->log_model->log_mail(array('message'=>$mensagem,'emails'=>$email_contact,'subject'=>$assunto));
		
		if(ENVIRONMENT == 'development'){
			return true;
		}
		
		if($this->send()){
			return true;
		}
		
		return false;
	}
}
