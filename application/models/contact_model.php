<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class contact_model extends CI_Model{

	var $tablename = "contact";

	public final function __construct()
	{
		parent::__construct();
	}

	/*
	 * Genéricas
	*/
	function getAll( $where=false, $content=false){

		if($content==false)
			$this->db->select('*');
		else
			$this->db->select($content);

		$this->db->from($this->tablename);

		if($where!=false)
			$this->db->where($where);
	
		$query = $this->db->get();

		if($query->num_rows() > 0)
			return $query->row_array();
		else
			return false;
	}

	function save()
	{
		$data = array(
			'name'		=> $this->input->post('name', FALSE),
			'email'		=> $this->input->post('email', FALSE),
			'subject'		=> $this->input->post('subject', FALSE),
			'message'	=> $this->input->post('message', FALSE),
			'status_id'	=> 1
		);
		
		if($this->sendMail($data)){
			return true;
		}
		
        return FALSE;
	}
	
	function lista($pagina, $where = array())
	{
		if($where){
			$where = base64_decode($where);
			
			parse_str($where, $where);
			
			if(!empty($where['id'])){
				$this->db->where('id', $where['id']);
			}
			
			if(!empty($where['name'])){
				$this->db->like('name' , $where['name']);
			}
			
			if(!empty($where['email'])){
				$this->db->like('email' , $where['email']);
			}
			
			if(!empty($where['phone'])){
				$this->db->like('phone' , $where['phone']);
			}
			
			if(!empty($where['city'])){
				$this->db->like('city' , $where['city']);
			}
			
			if(!empty($where['subject'])){
				$this->db->like('subject', $where['subject']);
			}
			
			if(!empty($where['message'])){
				$this->db->like('message', $where['message']);
			}
			
			if(!empty($where['status_id'])){
				$this->db->where('status_id', $where['status_id']);
			}
		}
		
		$this->db->select('*');
        $this->db->from($this->tablename);
		$this->db->where('status_id', 1);
		$this->db->order_by('id', 'desc');
		
		$inicio = ($pagina*10)-10;
        $this->db->limit(10,$inicio);
		
        $query = $this->db->get();
		
		return $query->result_array();
	}

	function total($where = array())
	{
		if($where){
			$where = base64_decode($where);
			
			parse_str($where, $where);
			
			if(!empty($where['id'])){
				$this->db->where('id', $where['id']);
			}
			
			if(!empty($where['name'])){
				$this->db->like('name' , $where['name']);
			}
			
			if(!empty($where['email'])){
				$this->db->like('email' , $where['email']);
			}
			
			if(!empty($where['phone'])){
				$this->db->like('phone' , $where['phone']);
			}
			
			if(!empty($where['city'])){
				$this->db->like('city' , $where['city']);
			}
			
			if(!empty($where['subject'])){
				$this->db->like('subject', $where['subject']);
			}
			
			if(!empty($where['message'])){
				$this->db->like('message', $where['message']);
			}
			
			if(!empty($where['status_id'])){
				$this->db->where('status_id', $where['status_id']);
			}
		}
		
 		$this->db->select('*');
        $this->db->from($this->tablename);
		$this->db->where('status_id', 1);
		
		return $this->db->get()->num_rows();
	}
	
	public final function change_status($id, $post)
	{
		return $this->db->where(array('id' => $id))->update($this->tablename, array('status_id' => $post, 'updated_in' => date('Y-m-d H:i:s')));
	}
	
	public final function sendMail($data)
	{
					
		$mensagem = '<html><head></head><body>
		Nome:       ' . $data['name'] . ' <br />
		E-mail:     ' . $data['email'] . ' <br />
		Assunto:     ' . $data['subject'] . ' <br />		
		Mensagem:   ' . $data['message'] . ' <br />
		</body></html>';
		
		// envia email
		if($this->email->envia(EMAIL_CONTATO, 'Contato a partir do site',$mensagem,false,$data['email'])){
			return true;
		}
		
		return false;
		
		
	}
}