<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class log_model extends CI_Model{

	var $tablename = "log_mailSend";

	public final function __construct()
	{
		parent::__construct();
	}

	/*
	 * Genéricas
	*/
	function getAll( $where=false, $content=false, $row = true)
	{
		if($content==false)
			$this->db->select('*');
		else
			$this->db->select($content);

		$this->db->from($this->tablename);

		if($where!=false)
			$this->db->where($where);

		$this->db->order_by('subject','ASC');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			if($row){
				return $query->row_array();
			}
			
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	

	
	function lista($pagina, $where = array(), $order)
	{
		if($where){
			$where = base64_decode($where);
			
			parse_str($where, $where);
			
			if(!empty($where['id'])){
				$this->db->where('id', $where['id']);
			}
			
			if(!empty($where['name'])){
				$this->db->like('subject' , $where['name']);
			}
			
			if(!empty($where['email'])){
				$this->db->like('emails', $where['email']);
			}
			
			if(!empty($where['status_id'])){
				
				$where['status_id'] = ($where['status_id'] == 2) ? 0 : $where['status_id'];
				
				$this->db->where('status_id', $where['status_id']);
			}
		}
		
		$this->db->select('*');
        $this->db->from($this->tablename);
		
		$inicio = ($pagina*10)-10;
        $this->db->limit(10,$inicio);
		
		switch($order['key']){
			case "id":
				$this->db->order_by('id', $order['order']);
			break;
			case "name":
				$this->db->order_by('subject', $order['order']);
			break;
			case "email":
				$this->db->order_by('emails', $order['order']);
			break;
			case "status":
				$this->db->order_by('status_id', $order['order']);
			break;
			default:
			$this->db->order_by('subject','ASC');
			break;
		}
		
        $query = $this->db->get();
		
		return $query->result_array();
	}

	function total($where = array())
	{
		if($where){
			$where = base64_decode($where);
			
			parse_str($where, $where);
			
			if(!empty($where['id'])){
				$this->db->where('id', $where['id']);
			}
			
			if(!empty($where['name'])){
				$this->db->like('subject' , $where['name']);
			}
			
			if(!empty($where['email'])){
				$this->db->like('emails', $where['email']);
			}
			
			if(!empty($where['status_id'])){
				$this->db->where('status_id', $where['status_id']);
			}
		}
		
 		$this->db->select('*');
        $this->db->from($this->tablename);
		
		return $this->db->get()->num_rows();
	}
	

	
	function log_mail($content)
	{		
			
		$data = array(
			'hash'			=> random_string('unique'),	
			'subject'	    => $content['subject'],
			'message'	    	=> $content['message'],
			'emails'		=> (is_array($content['emails']))?implode(",",$content['emails']):$content['emails'],
			'created_in'		=> date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('log_mailSend', $data)){
			$this->insert_id = $this->db->insert_id();
			
			return true;
		}
		
        return FALSE;
	}

	
	
}