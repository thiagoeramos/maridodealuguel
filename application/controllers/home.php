<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	private $keyword;
	private $data;
	
	private $validation = array(
		array(
			'field'	=> 'name',
			'label'	=> 'Nome',
			'rules' => 'trim|required',
		),
		array(
			'field'	=> 'email',
			'label'	=> 'Email',
			'rules' => 'trim|required|valid_email',
		),
		array(
			'field'	=> 'subject',
			'label'	=> 'Assunto',
			'rules' => 'trim|required',
		),	
		array(
			'field'	=> 'message',
			'label'	=> 'Mensagem',
			'rules' => 'trim|required',
		),
	);
	
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();	
		$this->load->model("work_model");
		$this->load->helper('text');
		$this->load->model('contact_model', 'contact');
		
		
	}

	public final function render($method, $data = array())
	{

		$this->load->view('frontend/common/header', $this->data);
		$this->load->view('frontend/'. $method, $this->data);
		$this->load->view('frontend/common/footer', $this->data);

	}

	public function index()
	{
		$this->data['works']		= $this->work_model->getAll(array('work.status_id' => 1), 'work.*', false, true);
		
		
		$this->render($this->router->method);
	}

	public function servicos()
	{
		$this->data['works']		= $this->work_model->getAll(array('work.status_id' => 1), 'work.*', false, true);
		$this->render($this->router->method);	
	}

	public function servicosinterna($id)
	{
                  $this->data['row']  = $this->work_model->getAll(array('id' => $id));
		
	
        
		$this->render($this->router->method);	
	}

	
	    
	public function atendimentocorporativo()
	{
		$this->render($this->router->method);	
	}
	
	public final function contato()
	{	
		$this->data['title']		= EMPRESA.' - '.ucfirst($this->router->method);
		
		$robot = $this->agent->robot();
					
		if($_POST and !empty($_POST['email']) and empty($robot)){
			
			$this->form_validation->set_rules($this->validation);
			
			if($this->form_validation->run() === TRUE){
				
				if($this->contact->save()){
					echo 'Formulário enviado com Sucesso!';
				}else{
					echo 'Erro ao enviar formulário';
				}
			}else{
				echo validation_errors();
			}
			
			exit;
		}
		
		$this->render($this->router->method);
	}

	public function trabalheconosco()
	{
		$this->render($this->router->method);	
	}

	
}