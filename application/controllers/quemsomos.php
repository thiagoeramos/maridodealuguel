<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Quemsomos extends CI_Controller {


	private $keyword;
	private $data;
	
	function __construct()
	{
		parent::__construct();
		
		$this->data = array();
		
	}

	public final function render($method, $data = array())
	{

		$this->load->view('frontend/common/header', $this->data);
		$this->load->view('frontend/'. $method, $this->data);
		$this->load->view('frontend/common/footer', $this->data);

	}

	public function index()
	{
		$this->render($this->router->method);
	}

	public function quemsomos()
	{
		$this->render($this->router->method);	
	}

	public function missao()
	{
		$this->render($this->router->method);
	}
	
}