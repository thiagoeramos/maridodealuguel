<?php defined('BASEPATH') OR exit('No direct script access allowed');

class work extends CI_Controller {
	
	private $validation = array(
		array(
			'field'	=> 'name',
			'label'	=> 'Nome',
			'rules' => 'trim|required',
		),
	);
	
	private $data;
	
	function __construct()
	{
		parent::__construct();
		$this->security_model->is_logged();
		
		$this->load->model("work_model", "dm");
		
		$this->data = array();
	}
	
	public final function render($method)
	{
		$this->load->view('dashboard/'.$this->router->class.'/'.$method, $this->data);
	}
	
	public function index($pagina = 1, $busca = false)
	{
		if($busca == 'asc' or $busca == 'desc' or !base64_decode($busca, true)){
			$busca = 0;
		}
		
		if(intval($pagina) == 0){
			
			$pagina = 1;
		}
		
		$this->data['ordered']['key']	= $this->uri->segment(5);
		$this->data['ordered']['order']	= $this->uri->segment(6);	
		$this->data['ordered']['order']	= ($this->data['ordered']['order']) ? $this->data['ordered']['order'] : 'desc';
		
		if($this->data['ordered']['order'] == "desc"){
			$this->data['ordered']['order'] = 'asc';
		}else if($this->data['ordered']['order'] == 'asc'){
			$this->data['ordered']['order'] = 'desc';
		}
		
		$this->data['busca']			= $busca;
		$this->data['list']				= $this->dm->lista($pagina, $busca, $this->data['ordered']);
		$this->data['url']				= $this->router->class.'/index/'.$pagina;
		
		if($busca !== false){
			$this->data['paginacao']	= pagination($pagina, $this->dm->total($busca), $this->router->class.'/index', $busca);
		}else{
			$this->data['paginacao']	= pagination($pagina, $this->dm->total(), $this->router->class.'/index');
		}
		
		if(base64_decode($busca, true)){
			
			$busca = base64_decode($busca);
			parse_str($busca, $busca);
			$busca = array_map('trim', $busca);
			
			$this->data['search']['id']			= @$busca['id'];
			$this->data['search']['name']		= @$busca['name'];
			$this->data['search']['status_id']	= @$busca['status_id'];
		}
		
		$this->render($this->router->method);
	}

	public function create()
	{
		$this->form_validation->set_rules($this->validation);
		
		if($_POST && $this->form_validation->run() === TRUE){
			
			if($_FILES['image']['error'] == 0){
				$result			= $this->uploadArchives($_FILES, 'assets/uploads/'.$this->router->class.'/', 'image');
				$_POST['image']	= $result['url'];
			}
			
			if($_FILES['thumbnail']['error'] == 0){
				$result				= $this->uploadArchives($_FILES, 'assets/uploads/'.$this->router->class.'/', 'thumbnail');
				$_POST['thumbnail']	= $result['url'];
			}
			
			if($this->dm->save()){
				$this->session->set_userdata('mensagem', array('mensagem' => 'Serviço salvo com sucesso', 'retorno' => true));
			}else{
				$this->session->set_userdata('mensagem', array('mensagem' => 'Erro ao salvar serviço', 'retorno' => false));
			}
			
			redirect($this->router->class);
		}
		
		$this->render($this->router->method);
	}
	
	public function update($id)
	{
		$this->data['row']	= $this->dm->getAll(array('id' => $id));		
				
		$this->form_validation->set_rules($this->validation);
		
		if($_POST && $this->form_validation->run() === TRUE){
			
			if($_FILES['image']['error'] != 0){
				if($_POST['delete_img'] == '1'){
					$_POST['image']	= '';
				}else{
					$_POST['image']	= $this->data['row']['image'];
				}
			}else{
				$result			= $this->uploadArchives($_FILES,'assets/uploads/'.$this->router->class.'/', 'image');
				$_POST['image']	= $result['url'];
			}
			
			if($_FILES['thumbnail']['error'] != 0){
				if($_POST['delete_thumbnail'] == '1'){
					$_POST['thumbnail']	= '';
				}else{
					$_POST['thumbnail']	= $this->data['row']['thumbnail'];
				}
			}else{
				$result				= $this->uploadArchives($_FILES,'assets/uploads/'.$this->router->class.'/', 'thumbnail');
				$_POST['thumbnail']	= $result['url'];
			}
			
			if($this->dm->save($id)){
				$this->session->set_userdata('mensagem', array('mensagem' => 'Serviço editado com sucesso', 'retorno' => true));
			}else{
				$this->session->set_userdata('mensagem', array('mensagem' => 'Erro ao editar serviço', 'retorno' => false));
			}
			
			redirect($this->router->class);
		}
		
		$this->render($this->router->method);
	}
	
	public function delete($id)
	{
		if($this->dm->change_status($id, '0')){
			$this->session->set_userdata('mensagem', array('mensagem' => 'Serviço inativado com sucesso', 'retorno' => true));
		}else{
			$this->session->set_userdata('mensagem', array('mensagem' => 'Erro ao inativar serviço', 'retorno' => false));
		}
		
		redirect($this->router->class);
	}
	
	public function active($id)
	{
		if($this->dm->change_status($id, '1')){
			$this->session->set_userdata('mensagem', array('mensagem' => 'Serviço ativado com sucesso', 'retorno' => true));
		}else{
			$this->session->set_userdata('mensagem', array('mensagem' => 'Erro ao ativar serviço', 'retorno' => false));
		}
		
		redirect($this->router->class);
	}
	
	public function uploadArchives($file, $path, $name)
	{
		$config['upload_path']	= $path;
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0777, true);
		}
		
		$config['allowed_types']	= '*';
		$valid_images				= array('jpg','JPG','JPEG','jpeg','GIF','gif','png','PNG','BMP','bmp');
		$valid_chars_regex 			= '.A-Z0-9_!@#$%^&()+={}\[\]\',~`-';
		$config['max_size'] 		= '2048';
		$config['overwrite']  		= FALSE;
		$config['encrypt_name'] 	= FALSE;
		$config['file_name']		= preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($file[$name]['name']));
		$config['file_name']		= removeAcentos($config['file_name']);
		
		$this->load->library('upload', $config);
		
		//Faz o upload
		
		if($this->upload->do_upload($name)){
			$upload_data 	= $this->upload->data();
			$file_new_name	= $upload_data['file_name'];
			$extension 		= getExt($file_new_name);
			$size 			= getSizeArchive($config['upload_path'].$file_new_name);
			
			if(in_array($extension,$valid_images)){
				return array('status' => 'ok', 'url' => $file_new_name, 'ext' => $extension, 'size' => $size);
			}else{
				return array('status' => 'Arquivo não suportado');
			}
		}else{
			echo $this->upload->display_errors();
		}
		
		return false;
	}
}