<?php defined('BASEPATH') OR exit('No direct script access allowed');

class logs_email extends CI_Controller {
	
	private $validation = array(
		array(
			'field'	=> 'name',
			'label'	=> 'Nome',
			'rules' => 'trim|required',
		),
	);
	
	private $data;
	
	function __construct()
	{
		parent::__construct();
		$this->security_model->is_logged();
		
		$this->load->model("log_model", "dm");
		
		$this->data = array();
		
	}
	
	public final function render($method)
	{
		$this->load->view('dashboard/'.$this->router->class.'/'.$method, $this->data);
	}
	
	public function index($pagina = 1, $busca = false)
	{
		if($busca == 'asc' or $busca == 'desc' or !base64_decode($busca, true)){
			$busca = 0;
		}
		
		if(intval($pagina) == 0){
			
			$pagina = 1;
		}
		
		$this->data['ordered']['key']	= $this->uri->segment(5);
		$this->data['ordered']['order']	= $this->uri->segment(6);	
		$this->data['ordered']['order']	= ($this->data['ordered']['order']) ? $this->data['ordered']['order'] : 'desc';
		
		if($this->data['ordered']['order'] == "desc"){
			$this->data['ordered']['order'] = 'asc';
		}else if($this->data['ordered']['order'] == 'asc'){
			$this->data['ordered']['order'] = 'desc';
		}
		
		$this->data['busca']			= $busca;
		$this->data['list']				= $this->dm->lista($pagina, $busca, $this->data['ordered']);
		$this->data['url']				= $this->router->class.'/index/'.$pagina;
		
		if($busca !== false){
			$this->data['paginacao']	= pagination($pagina, $this->dm->total($busca), $this->router->class.'/index', $busca);
		}else{
			$this->data['paginacao']	= pagination($pagina, $this->dm->total(), $this->router->class.'/index');
		}
		
		if(base64_decode($busca, true)){
			
			$busca = base64_decode($busca);
			parse_str($busca, $busca);
			$busca = array_map('trim', $busca);
			
			$this->data['search']['id']			= @$busca['id'];
			$this->data['search']['name']		= @$busca['name'];
			$this->data['search']['status_id']	= @$busca['status_id'];
		}
		
		$this->render($this->router->method);
	}

	
	public final function visualizar($id = false, $hash = false)
	{		
		$this->data['row']	= $this->dm->getAll(array('id' => $id));
		
				$this->render($this->router->method);
	}

	
}