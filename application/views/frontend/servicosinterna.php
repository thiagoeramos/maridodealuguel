 <div class="offset"></div>

<div class="light-wrapper">
    <div class="container inner" style="min-height:730px;">

        <div class="row">
            <div class="col-md-8">
                <div class="owl-carousel image-slider custom-controls">
                    <div class="item"><img src="<?=base_url('assets/uploads/work/'.$row['image']);?>" alt="" /></div>
                </div>
            </div>
            <div class="col-md-4">
                <h1><?=$row['name']?></h1><br />
                <p><?=$row['description']?></p>                        
            </div>
        </div>
        
        
    </div>
</div>