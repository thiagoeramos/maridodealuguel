
  <div class="offset"></div>
  <div class="light-wrapper" id="slider-home"> 
    <div class="tp-banner-container revolution" style="height:382px;">
      <div class="tp-banner" style="height:382px;">
        <ul>
          <li data-transition="fade"> <img src="<?=base_url('resources/images/banco-de-imagens/hidraulica/banner/banner_hidraulica.jpg');?>" alt="" data-bgfit="cover" data-bgposition="right center" data-bgrepeat="no-repeat" height="400" />
            <div class="tp-caption title sfb" data-x="left" data-y="40" data-speed="900" data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
               <a href="<?=site_url('servicos/detalhes/6');?>"> 
                    <p style="font-size:25px; color:#575656;">
                        Hidráulica<br />
                        <span style="color:#e30613;font-size: 42px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">Responsabilidade e qualidade<br /> também em instalações<br /> hidráulicas em geral <br /></span>
                         <span style="color:#575656;font-size: 22px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">                             
                                    Veja mais +
                           </span>
                    </p>
               </a>
            </div>
          </li>         
          <li data-transition="fade"> <img src="<?=base_url('resources/images/banco-de-imagens/reformas/banner/banner_Brisbane-builder.jpg');?>" alt="" data-bgfit="cover" data-bgposition="left center" data-bgrepeat="no-repeat" height="500" />
            <div class="tp-caption title sfb" data-x="left" data-y="40" data-speed="900" data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
              <a href="<?=site_url('servicos/detalhes/2');?>"> 
                    <p style="font-size:25px; color:#575656;">
                        Acabamento e Reformas<br />
                        <span style="color:#e30613;font-size: 42px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">Serviços realizados <br />com total garantia em <br /> Reformas e Manutenção Predial <br /></span>
                         <span style="color:#575656;font-size: 22px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">                             
                                    Veja mais +
                           </span>
                    </p>
              </a>
            </div>
          </li>
          <li data-transition="fade"> <img src="<?=base_url('resources/images/banco-de-imagens/ar-condicionado/banner/sli2.jpg');?>" alt="" data-bgfit="cover" data-bgposition="left center" data-bgrepeat="no-repeat" height="500" />
            <div class="tp-caption title sfb" data-x="left" data-y="40" data-speed="900" data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
                <a href="<?=site_url('servicos/detalhes/4');?>"> 
                    <p style="font-size:25px; color:#575656;">
                        Instalação de Ar Condicionado<br />
                        <span style="color:#e30613;font-size: 42px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">Manutenção e Instalação<br /> rápida e eficaz visando<br />  o seu bem estar <br /></span>
                         <span style="color:#575656;font-size: 22px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">                             
                                    Veja mais +
                           </span>
                   </p>
                </a>
            </div>
          </li>
          <li data-transition="fade"> <img src="<?=base_url('resources/images/banco-de-imagens/eletrica/banner/emendar-fios.jpg');?>" alt="" data-bgfit="cover" data-bgposition="left center" data-bgrepeat="no-repeat" height="500" />
            <div class="tp-caption title sfb" data-x="left" data-y="40" data-speed="900" data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
                <a href="<?=site_url('servicos/detalhes/3');?>"> 
                        <p style="font-size:25px; color:#575656;">
                            Instalações Elétricas<br />

                                <span style="color:#e30613;font-size: 42px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">
                                   Prestatação de Serviços <br /> de elétrica em toda São Paulo<br />
                               </span>


                                <span style="color:#575656;font-size: 22px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">                             
                                        Veja mais +
                               </span>
                       </p>
                  </a>
            </div>
          </li>
           <li data-transition="fade"> <img src="<?=base_url('resources/images/banco-de-imagens/pintura/banner/pintura_banner.jpg');?>" alt="" data-bgfit="cover" data-bgposition="left center" data-bgrepeat="no-repeat" height="500" />
            <div class="tp-caption title sfb" data-x="left" data-y="40" data-speed="900" data-start="500" data-endspeed="100" data-easing="Sine.easeOut">
                <a href="<?=site_url('servicos/detalhes/5');?>"> 
                        <p style="font-size:25px; color:#575656;">
                            Pintura e Textura<br />

                                <span style="color:#e30613;font-size: 42px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">
                                   Prestatação de Serviços <br /> de pintura em toda São Paulo<br />
                               </span>


                                <span style="color:#575656;font-size: 22px;line-height:50px; text-shadow: 0px 5px 15px #ffffff;">                             
                                        Veja mais +
                               </span>
                       </p>
                  </a>
            </div>
          </li>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
      </div>
      <!-- /.tp-banner --> 
    </div>
    <!-- /.tp-banner-container --> 
  </div>
  
  <div class="dark-wrapper">
    <div class="container inner" id="home-one">

      <div class="row">
        <div class="col-sm-12 col-md-12" style="background:#fff;height: 50px;margin-bottom: 20px; padding:15px 30px;color:#e30613; text-transform:uppercase;">SERVIÇOS</div>
      </div>

      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="lead bm25">Serviços Gerais em São Paulo </div>
          <p>
              Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus dictum mollis arcu non ullamcorper. Pellentesque iaculis lacinia sapien in vehicula. Praesent quis dictum lorem. Fusce nec vehicula ipsum. Suspendisse ut nunc sit amet ligula venenatis pretium. Nunc justo arcu, feugiat vitae viverra et, vehicula vel arcu. Aliquam mattis elit elit, sed luctus dolor malesuada id. Aenean non elit nibh. Integer iaculis bibendum est id maximus. Nulla leo odio, pellentesque sit amet facilisis ac, vehicula mattis diam.</p>
          
        </div>
        <div class="col-md-9 col-sm-12">
          <div class="owl-posts">
            <? if($works){                        
                foreach($works as $work){
            ?>
              <div class="item post">
                
                  <?
                  if(file_exists('assets/uploads/work/'.$work['thumbnail'])){
                    ?>
                    <figure>
                        <a href="<?=site_url('servicos/detalhes/'.$work['id']);?>" title="<?=ucwords($work['name']);?>" >
                        <div class="text-overlay">
                        <div class="info">Ver mais</div>
                        </div>                                
                        <img src="<?=base_url('assets/uploads/work/'.$work['thumbnail']);?>" alt="<?=ucwords($work['name']);?>" />
                        </a>
                    </figure>
                <?
                  }
                ?>  
             
                <div class="post-content">
                  <h3 class="post-title"><a href="<?=site_url('servicos/detalhes/'.$work['id']);?>" title="<?=ucwords($work['name']);?>"><?=ucwords($work['name']);?></a></h3>
                  <p><?=word_limiter($work['description'],20);?>&nbsp; <a href="<?=site_url('servicos/detalhes/'.$work['id']);?>" title="<?=ucwords($work['name']);?>">Veja mais</a></p>
                </div>
              </div>
           <?
                  }
            }
            ?>
          </div>
        </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.dark-wrapper -->
