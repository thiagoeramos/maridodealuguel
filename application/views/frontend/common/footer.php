<footer class="black-wrapper" style="margin-top:60px;">
    <div class="container inner">
      <div class="row">
        <div class="col-sm-4">
          <div class="widget"> 
            <p><span style="color:#e30613;">CELSO MARIDO DE ALUGUEL</span><br />
                Aliquam mattis elit elit, sed luctus dolor malesuada id. Aenean non elit nibh. Integer iaculis bibendum est id maximus. 
                Nulla leo odio, pellentesque sit amet facilisis ac, vehicula mattis diam.
                Vestibulum ipsum nisi, semper vitae nulla a, porta cursus mi. Quisque non purus at purus aliquam posuere id finibus eros.
                In accumsan velit at massa ultrices, vel vestibulum justo placerat. 
                </p>
          </div>
          <!-- /.widget -->
          
          
        </div>
        <!-- /col -->
        
        <div class="col-sm-4">
            
          <ul style="list-style: none; margin-top: 40px; ">
            <li><a href="<?=site_url('quem-somos');?>">SOBRE</a></li>         
            <li><a href="<?=site_url('servicos');?>">SERVIÇOS</a></li>  
            <li><a href="<?=site_url('contato');?>">CONTATO</a></li>
            <li><a href="<?=site_url('trabalhe-conosco');?>">TRABALHE CONOSCO</a></li>
          </ul>
        </div>
        <!-- /col -->
        
        <div class="col-sm-4">
          <div class="widget">
            <div class="contact-info">
              <i class="icon-location"></i> Rua Santa Mônica, 134 
              Vila Paraíso - Brasil - Guarulhos/SP <br />
              <i class="icon-phone"></i>Fone: +55 11 99331-6349 <br>
              <i class="icon-mail"></i> <a href="mailto:thiagoevangelista.contato@gmail.com">E-mail: thiagoevangelista.contato@gmail.com</a> <br />
              <div class="social">
              <a href="http://www.facebook.com/celsomaridodealuguel" class="facebook"></a>
              </div>
            </div>
          </div>
          <!-- /.widget -->
          
        </div>
        <!-- /col --> 
      </div>
      <!-- /.row --> 
    </div>
    <!-- .container -->
    
    
  </footer>
  <!-- /footer --> 
  
</div>
<!-- /.body-wrapper --> 
<script src="<?=base_url('resources/js/jquery.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/bootstrap.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/twitter-bootstrap-hover-dropdown.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.themepunch.plugins.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.themepunch.revolution.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.themepunch.tools.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/retina.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/owl.carousel.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.isotope.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.fancybox.pack.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.2');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.0');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.fitvids.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.easytabs.min.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/jquery.slickforms.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/google-code-prettify/prettify.js');?>" type="text/javascript"></script> 
<script src="<?=base_url('resources/js/scripts.js');?>" type="text/javascript"></script>
</body>
</html>