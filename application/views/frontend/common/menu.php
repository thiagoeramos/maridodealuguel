<div id="preloader"><div id="status"><div class="spinner"></div></div></div>
<div class="body-wrapper">
  <div class="navbar basic default">
    <div class="navbar-header">
      <div class="container">
        <div class="basic-wrapper"> 
          <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i></i></a>
          <a class="navbar-brand" href="<?=site_url();?>"><img src="<?=base_url('resources/images/logotipo.png');?>" alt="Clínica Oriental" height="10" class="img-responsive" id="logo-image" /></a>
        </div>
        <nav class="collapse navbar-collapse pull-right">
          <ul class="nav navbar-nav" id="menu-ie">           
            <li><a href="<?=site_url('quem-somos');?>">SOBRE</a></li>         
            <li><a href="<?=site_url('servicos');?>">SERVIÇOS</a></li>
<!--            <li class="dropdown"><a href="javascript:" class="dropdown-toggle js-activated">ATENDIMENTO CORPORATIVO</a>
              <ul class="dropdown-menu">
                <li><a href="<?=site_url('eventos');?>">Eventos</a></li>
                <li><a href="<?=site_url('nossos-clientes');?>">Nossos Clientes</a></li>
                <li><a href="<?=site_url('massagem-incompany');?>">Massagem In-Company</a></li>
              </ul>
            </li>-->
            <li><a href="<?=site_url('contato');?>">CONTATO</a></li>

          </ul>
        </nav>
        <!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <!--/.navbar -->