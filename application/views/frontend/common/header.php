<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="resources/images/favicon.ico">
<title>Celso Marido de Aluguel</title>
<!-- Bootstrap core CSS -->
<link href="<?=base_url('resources/css/bootstrap.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/css/settings.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/css/owl.carousel.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/js/google-code-prettify/prettify.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/js/fancybox/jquery.fancybox.css');?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url('resources/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.2');?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('resources/css/style.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/css/color/red.css');?>" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Sorts+Mill+Goudy:400,400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href="<?=base_url('resources/type/fontello.css');?>" rel="stylesheet">
<link href="<?=base_url('resources/type/budicons.css');?>" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?=base_url('resources/js/html5shiv.js');?>"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
</head>
<body class="full-layout">
	<?php require_once('menu.php'); ?>