 <div class="offset"></div>

<div class="light-wrapper">
    <div class="container inner">

    <div class="portfolio fix-portfolio grid-portfolio">
        <h1>Serviços</h1><br />
        
        <ul class="items col4">
            <? if($works){ foreach($works as $work){ ?>
            <li class="item thumb graphic">
              <a href="<?=site_url('servicos/detalhes/'.$work['id']);?>">
                <? if(file_exists('assets/uploads/work/'.$work['thumbnail'])){ ?>
                    <figure>
                        <a href="<?=site_url('servicos/detalhes/'.$work['id']);?>" title="<?=ucwords($work['name']);?>" class="fancybox-media" data-rel="grid-portfolio">
                        <div class="text-overlay">
                            <div class="info">Ver</div>
                        </div>                           
                        <img src="<?=base_url('assets/uploads/work/'.$work['thumbnail']);?>" alt="<?=ucwords($work['name']);?>" /></a>
                    </figure>
                <? } ?> 
                    <div class="box">
                        <h3 class="post-title"><a href="<?=site_url('servicos/detalhes/'.$work['id']);?>"><?=ucwords($work['name']);?></a></h3>
                    </div>
                </a>
            </li>
            <? } } ?>         
        </ul>
        <!-- /.items --> 
      </div>
      <!-- /.portfolio -->
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.light-wrapper -->