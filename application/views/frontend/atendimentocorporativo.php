  <div class="offset"></div>
  
  <div class="light-wrapper">
    <div class="container inner">
      <div class="row">
        <div class="col-sm-4">
          <figure><img src="<?=base_url('resources/images/in-company.jpg');?>" alt="" /></figure><br />
        </div>
        <div class="col-sm-8">
          <h4>Atendimento Corporativo</h4>
          <p>A Clinica Oriental leva os seus profissionais até a sua empresa com o objetivo de prevenir o estresse e tratar doenças ocupacionais como LER* e DORT**, e com isso motivar a equipe, aumentando a produtividade e a mantendo afastada dos problemas de saúde.</p>
          <p>As técnicas mais procuradas pelas empresas são a Quick Massage e a Reflexologia, com duração de 15 e 20 minutos, respectivamente, com atendimento individualizado. Porém, a Clinica Oriental oferece também outras técnicas como Spiral Taping, Acupuntura, Drenagem Linfática e as massagens Clássica, Shiatsu e Anmá.</p>
          <p>Cada colaborador atendido responde a um questionário sobre os motivos que o levaram a procurar o atendimento e a melhora percebida durante o tratamento. Esses dados são compilados sempre no final de cada mês e apresentados para a empresa. Investindo na qualidade de vida dos colaboradores, a empresa tem como retorno a sua satisfação, motivação e aumento de produtividade, diminuindo as despesas com relação a afastamentos por motivo de saúde.</p>
          <p>Todos os nossos profissionais são altamente qualificados com formação técnica na EOMA (Escola Oriental de Massagem e Acupuntura) e preparados para trabalhar nos programas de Qualidade de Vida com o uso da massagem.</p>
          <p>Envie uma solicitação de orçamento clicando aqui, ou entre em contato pelo telefone: (11) 5592-1443.</p>
        </div>
      </div>
      <div class="divide50"></div>
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.light-wrapper -->