  <div class="offset"></div>
  
  <div class="light-wrapper">
    <div class="container inner">
      <div class="row">
        <div class="col-sm-4">
          <figure><img src="<?=base_url('resources/images/evento.jpg');?>" alt="" /></figure><br />
        </div>
        <div class="col-sm-8">
          <h4>Eventos</h4>
          <p>
            Além do atendimento In-Company, também atuamos em eventos de pequeno, médio e grande porte, tais como:<br /><br />
            - Eventos relacionados à Semana Interna de Prevenção de Acidentes de Trabalho (SIPAT)<br />
            - Congressos<br />
            - Feiras<br />
            - Ações Promocionais
          </p>
          <p>A equipe da Clínica Oriental está a postos para trazer um diferencial inovador para o seu evento!</p>
          <p>Para fazer uma solicitação de orçamento, por favor entre em contato pelo e-mail <a href="mailto:janete@grupooriental.com.br">janete@grupooriental.com.br</a> ou pelo telefone (11) 5592-1443.</p>
        </div>
      </div>
      <div class="divide50"></div>
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.light-wrapper -->