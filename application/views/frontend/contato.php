<div class="offset"></div>
  
  
  <div class="light-wrapper">
    <div class="container inner">
    
          
      <div class="row">
        <aside class="col-sm-4 sidebar left-sidebar lp20">
          <div class="sidebox widget">
            <div class="contact-info"> <i class="icon-location"></i> Rua Santa Mônica, 134 <br />Vila Paraíso - Brasil - Guarulhos/SP <br />
              <i class="icon-phone"></i>Fone: +55 11 99331-6349 <br />
              <i class="icon-mail"></i> <a href="contato@grupooriental.com.br"> E-mail: thiagoevangelista.contato@gmail.com </a> </div>
          </div>
          <!-- /.widget --> 
          
          <div class="sidebox widget">
            <h5 class="widget-title">Redes Sociais</h5>
            <ul class="social">
              <li><a href="http://www.facebook.com/celsomaridodealuguel"><i class="icon-s-facebook"></i></a></li>
            </ul>
          </div>
          <!-- /.widget -->
        </aside>
        <!-- /.col -->
        
        <div class="col-sm-8">
        <h3 class="section-title text-left">Fale Conosco</h3>
          <div class="form-container">
            <div class="response alert alert-success"></div>
             <form class="forms formContato" action="<?=site_url('contato');?>" method="post">
              <fieldset>
                <ol>
                  <li class="form-row text-input-row name-field">
                    <input type="text" name="name" class="text-input defaultText required" title="Nome (Obrigatório)"/>
                  </li>
                  <li class="form-row text-input-row email-field">
                    <input type="text" name="email" class="text-input defaultText required email" title="E-mail (Obrigatório)"/>
                  </li>
                  <li class="form-row text-input-row subject-field">
                    <select name="subject">
                        <option value="">Assunto (Obrigatório)</option>
                        <option value="Sugestões">Sugestões</option>
                        <option value="Elogios">Elogios</option>
                        <option value="Depoimentos">Depoimentos</option>
                        <option value="Reclamações">Reclamações</option>
                        <option value="Orçamentos">Orçamentos</option>
                        <option value="Outros">Outros</option>
                    </select>
                  </li>
                  <li class="form-row text-area-row">
                    <textarea name="message" class="text-area required" title="Mensagem"></textarea>
                  </li>
                  <li class="form-row hidden-row">
                    <input type="hidden" name="hidden" value="" />
                  </li>
                  <li class="nocomment">
                    <label for="nocomment">Leave This Field Empty</label>
                    <input id="nocomment" value="" name="nocomment" />
                  </li>
                  <li class="button-row">
                    <input type="submit" value="Enviar" name="submit" class="btn btn-submit bm0" />
                  </li>
                </ol>
                <input type="hidden" name="v_error" id="v-error" value="Required" />
                <input type="hidden" name="v_email" id="v-email" value="Enter a valid email" />
              </fieldset>
            </form>
          </div>
          <!-- /.form-container --> 
        </div>
        <!-- /.col -->
         
      </div>
      <!-- /.row -->
      
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.light-wrapper -->