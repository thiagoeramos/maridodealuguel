<? $this->load->view('dashboard/template/header');?>
<body>
	<? $this->load->view('dashboard/template/navbar');?>
	<? $this->load->view('dashboard/template/sidebar');?>
	
	<!-- main container -->
    <div class="content">
        <div id="pad-wrapper" class="new-user">
            <div class="row">
                <div class="col-md-12">
                    <h3>Visualizar Email</h3>
                </div>
            </div>
			<br /><br />
			<? if(validation_errors()){ ?>
<div class="alert alert-danger">
	<?php echo validation_errors(); ?>
</div>
<? } ?>

<div class="row form-wrapper">
	
	<!-- left column -->
	<div class="col-md-9 with-sidebar">
		
		<div class="container">
			
			<form class="new_user_form" method="post" enctype="multipart/form-data" >
				
				<div class="col-md-12 field-box">			
					<?=@$row['message'];?>
				</div>
				
				
				
			</form>
		</div>
	</div>

	<!-- side right column -->
	<div class="col-md-3 form-sidebar pull-right"></div>
</div>
        </div>
    </div>
    <!-- end main container -->
	<? $this->load->view('dashboard/template/footer');?>