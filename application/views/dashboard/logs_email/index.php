<? $this->load->view('dashboard/template/header');?>

<body>

	<? $this->load->view('dashboard/template/navbar');?>
	<? $this->load->view('dashboard/template/sidebar');?>

	<!-- main container -->
    <div class="content">

        <div id="pad-wrapper" class="users-list">
			
			<? if($this->session->userdata('mensagem')){

				$mensagem  = $this->session->userdata('mensagem');
				$class 		= 'erro';
				
				$this->session->unset_userdata('mensagem');
				
				if($mensagem['retorno'] == 1 || $mensagem['retorno'] == 'sucess'){
					$class = 'alert alert-success';
				}
				?>
				
				<div class="<?=$class;?>">
					<p><?=$mensagem['mensagem'];?></p>
				</div>
			
			<? } ?>
            
			<div class="row">
                <h2 class="col-md-10 col-sm-10 col-xs-10">Log de Emails enviados</h2> 
                <div class="col-md-2 col-sm-2 col-xs-2">
              
                </div>
            </div>
			<br />
			
            <?php if( $list!=false ){ ?>
            <!-- Users table -->
            <div class="row">
                <div class="col-md-12">
				  
				  <div class="panel panel-default">
					
					<?  $metodo = ($this->uri->segment(2) == NULL) ? 'index' : $this->uri->segment(2); ?>
					
                    <table class="table table-striped table-hover table-bordered table-condensed table-responsive">
                        <thead>
                            <tr>
								<th class="col-md-1"><a href="<?=site_url($url."/".$busca."/id/".$ordered['order']);?>"># <span class="grid20 pull-right  <?=($ordered['key'] == 'id') ? (($ordered['order'] == 'desc') ? 'seta-cima' : 'seta-baixo') : '';?>" />&nbsp;</span></a></th>
								<th class="col-md-2"><a href="<?=site_url($url."/".$busca."/name/".$ordered['order']);?>">Assunto <span class="grid20 pull-right  <?=($ordered['key'] == 'name') ? (($ordered['order'] == 'desc') ? 'seta-cima' : 'seta-baixo') : '';?>" />&nbsp;</span></a></th>
								<th class="col-md-2">Emails <span class="grid20 pull-right" /></span></th>
								<th class="col-md-2"><a href="<?=site_url($url."/".$busca."/status/".$ordered['order']);?>">Status <span class="grid20 pull-right  <?=($ordered['key'] == 'status') ? (($ordered['order'] == 'desc') ? 'seta-cima' : 'seta-baixo') : '';?>" />&nbsp;</span></a></th>
                                <th class="col-md-2">Ação</th>
                            </tr>
                        </thead>
                        <tbody>
							<tr>
								<form role="form" method="get" id="frmBusca" action="<?=site_url($this->uri->segment(1).'/'.$metodo.'/1');?>">
									<td>
										<input type="text" name="id" value="<?=@$search['id'];?>" placeholder="Filtrar por ID" class="form-control numeric" />
									</td>
									<td>
										<input type="text" name="name" value="<?=@$search['name'];?>" placeholder="Filtrar por Nome" class="form-control" />
									</td>
									<td>
										&nbsp;
									</td>
									<td>
										<select name="status_id" class="form-control" id="onChange">
											<option value="">Filtrar por Status</option>
											<?=select_status(@$search['status_id']);?>
										</select>
									</td>
								
									<td>
										<input type="submit" name="btSubmit" value="Filtrar" class="btn btn-primary" id="buscar" />
										<input type="button" name="btClear" value="Limpar" class="btn btn-info" id="limpa_busca" />
									</td>
								</form>
							</tr>
							
							<? foreach($list as $row){ ?>
							   <!-- row -->
							   <tr class="first">
									<td>
									   <?=$row['id'];?>
									</td>
									<td>
									   <?=$row['subject'];?>
									</td>
									<td>
									   <?=$row['emails'];?>
									</td>
									
									<td>
									   <?=status2txt($row['status_id']);?>
									</td>
									<td>
										
										<a class="btn btn-xs btn-warning" href="<?=site_url($this->router->class.'/visualizar/'.$row['id'].'/'.$row['hash']);?>"><i class="icon-search"></i>&nbsp;Visualizar</a> &nbsp;
										  
										
										
										
									</td>
							   </tr>
						   <?php }?>
                        </tbody>
                    </table>
					</div>
                </div>
            </div>

            <?php }else{ ?>
               <p> Nenhum serviço cadastrado, clique no botão acima para adicionar.</p>
            <?php } ?>

            <?=$paginacao?>

            <!-- end users table -->
        </div>
    </div>
    <!-- end main container -->

<? $this->load->view('dashboard/template/footer');?>