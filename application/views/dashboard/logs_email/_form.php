<? if(validation_errors()){ ?>
<div class="alert alert-danger">
	<?php echo validation_errors(); ?>
</div>
<? } ?>

<div class="row form-wrapper">
	
	<!-- left column -->
	<div class="col-md-9 with-sidebar">
		
		<div class="container">
			
			<form class="new_user_form" method="post" enctype="multipart/form-data" >
				
				<div class="col-md-12 field-box">
					<label>Nome:</label>
					<input class="form-control maiusculo" type="text" name="name" value="<?=set_value('name', @$row['name'], $this->input->post('name')); ?>" />
				</div>
				
				<div class="col-md-12 field-box">
					<label for="image" class="col-md-2 control-label">Foto:</label>
					<div class="col-md-8">
						<input type="file" name="image" placeholder="">
					</div>
					<? if(isset($row['image']) && strlen($row['image'])>4){ $row['image'] = explode('.', $row['image']); ?>
					<div class="col-md-11 field-box actions">
						<img id="thumb" src="<?=base_url('assets/uploads/'.$this->router->class.'/'.$row['image'][0].'_thumb.'.$row['image'][1]);?>" />
						<input type="button" class="btn btn-danger delete_image" value="Apagar Foto" />
						<input type="hidden" value="0" id="delete_img" name="delete_img" />
					</div>
					<? } ?>
				</div>
				
				<div class="col-md-12 field-box">
					<label for="description" class="col-md-2 control-label">Descrição:</label>
					<div class="col-md-8">
						<textarea name="description" class="form-control" rows="5"><?=set_value('description', @$row['description'], $this->input->post('description')); ?></textarea>
					</div>
				</div>
				
				
				<div class="col-md-12 field-box">
					<hr>
					<br /><br />
					<h2 style="margin-top:-25px;margin-bottom:15px;">Unidades</h2>
					
					<label for="course_type_id" class="col-md-2 control-label">Selecione a unidade:</label>
					<div class="col-md-8">
						<select class="select2" id="unit" name="unit" style="width:320px" >
						<?
							$unit_id = (isset($row['unit_id'])) ? $row['unit_id'] : 0;
							$unit_id = ($_POST) ? $_POST['unit_id'] : $unit_id;
							
							format_select($units, $unit_id);
						?>
						</select>
						
						<input id="add_unit" type="button" style="margin-left: 5px;" class="btn btn-default btn-ls pull-right" value="Adicionar" />
						
					</div>
					<br /><br />
					<hr>
					<table class="table table-bordered table-striped" id="table_unidade" style="background: #fff;">
						<thead>
							<tr>
								<th>Unidade</th>
								<th width="150">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							
							<? if(@$_POST['unit_id']){ foreach($_POST['unit_id'] as $k => $post){ ?>
								
								<tr>
									<td>
										<input name='unit_id[<?=$k?>]' type='hidden' value='<?=$post;?>'/>
										<input name='unit_name[<?=$k?>]' type='hidden' value='<?=$_POST['unit_name'][$k];?>'/>
										<span><?=$_POST['unit_name'][$k];?></span>
									</td>
									<td>
										<a href="javascript:" class="delete_unit btn btn-xs btn-default"><span class="glyphicon glyphicon-remove"></span></a>
									</td>
								</tr>
								
							<? } } ?>
							
							<? if (!$_POST and isset($row['unit_data'])){ $count = 1; foreach($row['unit_data'] as $unit){ ?>
								<tr>
									<td>
										<input name='unit_id[<?=$count?>]' type='hidden' value='<?=$unit['unit_id'];?>'/>
										<input name='unit_name[<?=$count?>]' type='hidden' value='<? foreach($units as $unit_data){ if($unit_data['id'] == $unit['unit_id']){ echo $unit_data['name']; } } ?>'/>
										<span><? foreach($units as $unit_data){ if($unit_data['id'] == $unit['unit_id']){ echo $unit_data['name']; } } ?></span>
									</td>
									<td>
										<a href='javascript:' class="delete_unit btn btn-xs btn-default"><span class="glyphicon glyphicon-remove"></span></a>
									</td>
								</tr>
							<? ++$count; }} ?>
						</tbody>
					</table>
					
					
				
				</div>
				
				<div class="col-md-11 field-box actions">
                    <input type="submit" class="btn btn-primary btn-ls" value="<?=($this->router->method == 'update') ? 'Editar' : 'Cadastrar';?>">
                    <span>OU</span>
                    <input type="reset" id="voltar" value="Cancelar" class="reset">
                </div>
			</form>
		</div>
	</div>

	<!-- side right column -->
	<div class="col-md-3 form-sidebar pull-right"></div>
</div>