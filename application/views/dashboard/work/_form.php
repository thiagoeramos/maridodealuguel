<? if(validation_errors()){ ?>
<div class="alert alert-danger">
	<?php echo validation_errors(); ?>
</div>
<? } ?>

<div class="row form-wrapper">
	
	<!-- left column -->
	<div class="col-md-9 with-sidebar">
		
		<div class="container">
			
			<form class="new_user_form" method="post" enctype="multipart/form-data" >
				
				<div class="col-md-12 field-box">
					<label>Nome:</label>
					<input class="form-control maiusculo" type="text" name="name" value="<?=set_value('name', @$row['name'], $this->input->post('name')); ?>" />
				</div>
				
				<div class="col-md-12 field-box">
					<label for="thumbnail" class="col-md-2 control-label">Thumbnail:</label>
					<div class="col-md-8">
						<input type="file" name="thumbnail" placeholder="">
					</div>
					<? if(isset($row['thumbnail']) && strlen($row['thumbnail'])>4){ ?>
					<div class="col-md-11 field-box actions">
						<img id="thumbnail" src="<?=base_url('assets/uploads/'.$this->router->class.'/'.$row['thumbnail']);?>" width="190" />
						<input type="button" class="btn btn-danger delete_thumbnail" value="Apagar Foto" />
						<input type="hidden" value="0" id="delete_thumbnail" name="delete_thumbnail" />
					</div>
					<? } ?>
				</div>
				
				<div class="col-md-12 field-box">
					<label for="image" class="col-md-2 control-label">Foto:</label>
					<div class="col-md-8">
						<input type="file" name="image" placeholder="">
					</div>
					<? if(isset($row['image']) && strlen($row['image'])>4){ ?>
					<div class="col-md-11 field-box actions">
						<img id="image" src="<?=base_url('assets/uploads/'.$this->router->class.'/'.$row['image']);?>" width="190" />
						<input type="button" class="btn btn-danger delete_image" value="Apagar Foto" />
						<input type="hidden" value="0" id="delete_img" name="delete_img" />
					</div>
					<? } ?>
				</div>
				
				<div class="col-md-12 field-box">
					<label>Gravidade de exibição:</label>
					<input class="form-control numeric" type="text" name="gravity" value="<?=($_POST) ? $_POST['gravity'] : @$row['gravity']; ?>" />
				</div>
				
				<div class="col-md-12 field-box">
					<label for="description" class="col-md-2 control-label">Descrição:</label>
					<div class="col-md-8">
						<textarea name="description" class="form-control" rows="5"><?=set_value('description', @$row['description'], $this->input->post('description')); ?></textarea>
					</div>
				</div>			
				
				<div class="col-md-11 field-box actions">
                    <input type="submit" class="btn btn-primary btn-ls" value="<?=($this->router->method == 'update') ? 'Editar' : 'Cadastrar';?>">
                    <span>OU</span>
                    <input type="reset" id="voltar" value="Cancelar" class="reset">
                </div>
			</form>
		</div>
	</div>

	<!-- side right column -->
	<div class="col-md-3 form-sidebar pull-right"></div>
</div>